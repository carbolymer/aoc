module Y2021.D06Spec where

import           Common
import           Data.List.Split             (splitOn)
import qualified Data.Map.Strict             as M
import           Test.Hspec

spec :: Spec
spec = do
  describe "runs exammple" $ do
    it "simulate 80 generations" $ do
      countFish (simulateNgenerations 80 $ histogram testInput) `shouldBe` 5934
    it "simulate 256 generations" $ do
      countFish (simulateNgenerations 256 $ histogram testInput) `shouldBe` 26984457539

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2021/d06.txt"
    let parsedInput :: FishSchool = histogram $ map read $ splitOn "," input
    it "---PART ONE---" $ do
      countFish (simulateNgenerations 80 parsedInput) `shouldBe` 360761
    it "---PART TWO---" $ do
      countFish (simulateNgenerations 256 parsedInput) `shouldBe` 1632779838045

type FishSchool = M.Map Int Int

countFish :: FishSchool -> Int
countFish = sum . M.elems

simulateNgenerations :: Int -> FishSchool -> FishSchool
simulateNgenerations n = applyN' n simulateFish

simulateFish :: FishSchool -> FishSchool
simulateFish !initialSchool = M.insertWith (+) 8 producing aged
  where producing = M.findWithDefault 0 0 initialSchool
        aged      = M.mapKeysWith (+) dec initialSchool
        dec !k    = if k > 0 then k - 1 else 6

testInput :: [Int]
testInput = [3,4,3,1,2]
