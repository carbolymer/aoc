module Y2022.D13Spec where

import Common
import Data.Aeson                    (FromJSON)
import Data.Aeson.Types              (FromJSON (parseJSON), Value (..))
import Data.ByteString.Lazy.Char8    (pack)
import Data.List.Split
import Data.Scientific               (floatingOrInteger)
import GHC.Exts                      (toList)
import Test.Hspec
import Text.InterpolatedString.Perl6
import Data.List (sort)

spec :: Spec
spec = do
  describe "validate parsing of signal" $ do
    it "parse list" $ do
      decodeFail "[1,2,3]" == Tree (Leaf <$> [1,2,3])
    it "parse nested list" $ do
      decodeFail "[1,[2],3]" == Tree [Leaf 1,Tree [Leaf 2],Leaf 3]
  describe "validate sample input" $ do
    it "parse input" $ do
      parseInput sampleInput `shouldBe`
        [(Tree [Leaf 1,Leaf 1,Leaf 3,Leaf 1,Leaf 1],Tree [Leaf 1,Leaf 1,Leaf 5,Leaf 1,Leaf 1])
        ,(Tree [Tree [Leaf 1],Tree [Leaf 2,Leaf 3,Leaf 4]],Tree [Tree [Leaf 1],Leaf 4])
        ,(Tree [Leaf 9],Tree [Tree [Leaf 8,Leaf 7,Leaf 6]])
        ,(Tree [Tree [Leaf 4,Leaf 4],Leaf 4,Leaf 4],Tree [Tree [Leaf 4,Leaf 4],Leaf 4,Leaf 4,Leaf 4])
        ,(Tree [Leaf 7,Leaf 7,Leaf 7,Leaf 7],Tree [Leaf 7,Leaf 7,Leaf 7])
        ,(Tree [],Tree [Leaf 3])
        ,(Tree [Tree [Tree []]],Tree [Tree []])
        ,(Tree [Leaf 1,Tree [Leaf 2,Tree [Leaf 3,Tree [Leaf 4,Tree [Leaf 5,Leaf 6,Leaf 7]]]],Leaf 8,Leaf 9],Tree [Leaf 1,Tree [Leaf 2,Tree [Leaf 3,Tree [Leaf 4,Tree [Leaf 5,Leaf 6,Leaf 0]]]],Leaf 8,Leaf 9])]
    it "sample part 1" $ do
      map (uncurry compare) (parseInput sampleInput) `shouldBe` [LT,LT,GT,LT,GT,LT,GT,GT]

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d13.txt"
    it "---PART ONE---" $ do
      let theSum = sum . map fst . filter ((== LT) . snd) . zip [1..] $ map (uncurry compare) (parseInput input) :: Int
      theSum `shouldBe` 5882
    it "---PART TWO---" $ do
      let isMarker signal = signal == Tree [Tree [Leaf 2]] || signal == Tree [Tree [Leaf 6]]
          markerIndices =
            map fst
            . filter (isMarker . snd)
            . zip [1..]
            . sort
            . map (decodeFail . pack)
            . (["[[2]]", "[[6]]"] <> )
            . filter (/= "")
            $ lines input
      markerIndices `shouldBe` [126, 198]
      product markerIndices `shouldBe` 24948

data Tree
  = Leaf Int
  | Tree [Tree]
  deriving (Eq, Show)

instance Ord Tree where
  (Leaf l1) `compare` (Leaf l2) = l1 `compare` l2
  (Tree t1) `compare` (Tree t2) = t1 `compare` t2
  l1@(Leaf _) `compare` (Tree t2) = [l1] `compare` t2
  (Tree t1) `compare` l2@(Leaf _) = t1 `compare` [l2]


instance FromJSON Tree where
  parseJSON (Number n) = case floatingOrInteger @Double n of
                           Left f  -> fail $ "not an int: " <> show f
                           Right i -> pure $ Leaf i
  parseJSON (Array a) = Tree <$> mapM parseJSON (toList a)
  parseJSON other = fail $ "cannot parse: " <> show other

parseInput :: String -> [(Tree, Tree)]
parseInput = map (toTree . lines) . splitOn "\n\n"
  where
    toTree :: [String] -> (Tree, Tree)
    toTree [one, two] = (decodeFail $ pack one, decodeFail $ pack two)
    toTree wtf        = error $ "yo wtf input: " <> show wtf

sampleInput :: String
sampleInput = [qc|[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]|]
