module Y2022.D10Spec where

import Common
import Control.Arrow
import Control.Monad.State.Strict
import Data.List.Split               (chunksOf)
import Test.Hspec
import Text.InterpolatedString.Perl6

spec :: Spec
spec = do
  describe "validate sample input" $ do
    it "read sampleInput" $ do
      take 5 (parseInput sampleInput) `shouldBe` [Addx 15, Addx (-11), Addx 6, Addx (-3), Addx 5]
    it "calculateSignalStrength sampleInput" $ do
      (findSignalStrengths . take 10 $ parseInput sampleInput) `shouldBe` [(0,1),(1,1),(2,16),(3,16),(4,5),(5,5),(6,11),(7,11),(8,8),(9,8),(10,13),(11,13),(12,12),(13,12),(14,4),(15,4),(16,17),(17,17),(18,21),(19,21)]
    it "sample part 1" $ do
      (sumSelectedCycles . findSignalStrengths $ parseInput sampleInput) `shouldBe` 13140
    it "sample part 2" $ do
      (renderImage . findSignalStrengths $ parseInput sampleInput) `shouldBe` sampleImage

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d10.txt"
    it "---PART ONE---" $ do
      (sumSelectedCycles . findSignalStrengths $ parseInput input) `shouldBe` 17940
    it "---PART TWO---" $ do
      let image = renderImage . findSignalStrengths $ parseInput input
      image `shouldBe`
        [ "####..##..###...##....##.####...##.####."
        , "...#.#..#.#..#.#..#....#.#.......#....#."
        , "..#..#....###..#..#....#.###.....#...#.."
        , ".#...#....#..#.####....#.#.......#..#..."
        , "#....#..#.#..#.#..#.#..#.#....#..#.#...."
        , "####..##..###..#..#..##..#.....##..####."]

data Cmd = Noop | Addx !Int
  deriving (Show, Eq, Ord)

parseInput :: String -> [Cmd]
parseInput = map parseLine . lines
  where
    parseLine "noop"                      = Noop
    parseLine ('a':'d':'d':'x':' ':value) = Addx (read value)
    parseLine other                       = error $ "cannot parse line: " <> other

findSignalStrengths :: [Cmd] -> [(Int, Int)]
findSignalStrengths cmds = reverse . (`execState` [(0, 1)]) $ mapM_ runCmd cmds
  where
    runCmd cmd = do
      case cmd of
        Noop -> strength >>= push
        Addx v -> do
          strength >>= push
          strength' <- strength
          push (strength' + v)
    strength = gets (snd . head)
    push signal = modify' $ \s -> (fst (head s) + 1, signal) : s

sumSelectedCycles :: [(Int, Int)] -> Int
sumSelectedCycles = sum . map (uncurry (*) . first (1 +)) . filter ((`elem` selectedCycles) . fst)
  where
    selectedCycles :: [Int]
    selectedCycles = map (\i -> i - 1) [20, 60, 100, 140,180, 220]


renderImage :: [(Int, Int)] -> [String]
renderImage =
  map drawLine
  . zipWith (\offset row -> map (\(p, x) -> (p - offset * 40, x)) row) [0..]
  . take 6
  . chunksOf 40
  where
    drawLine :: [(Int, Int)] -> String
    drawLine = foldl drawPixel (replicate 40 '.') . filter (\(p, x) -> abs (p - x) <= 1)

    drawPixel :: String -> (Int, Int) -> String
    drawPixel line (p, _) = updateAt p '#' line

sampleImage :: [String]
sampleImage = lines [qc|##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....|]

sampleInput :: String
sampleInput = [qc|addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop|]
