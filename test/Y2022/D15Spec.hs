module Y2022.D15Spec where

import Common
import Control.Concurrent            (getNumCapabilities)
import Control.Monad
import Control.Monad.State.Strict
import Data.Foldable                 hiding (toList)
import Data.Map.Strict               qualified as M
import Data.Maybe
import GHC.Exts
import Linear                        hiding (point, trace)
import Protolude                     (purer)
import Test.Hspec
import Text.InterpolatedString.Perl6
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer    qualified as L
import UnliftIO                      (pooledForConcurrentlyN)

spec :: Spec
spec = do
  describe "validate sample input" $ do
    it "sample part 1" $ do
      parseInput sampleInput `shouldBe`
          [(V2 2 18,V2 (-2) 15),(V2 9 16,V2 10 16),(V2 13 2,V2 15 3),(V2 12 14,V2 10 16),(V2 10 20,V2 10 16),(V2 14 17,V2 10 16),(V2 8 7,V2 2 10),(V2 2 0,V2 2 10),(V2 0 11,V2 2 10),(V2 20 14,V2 25 17),(V2 17 20,V2 21 22),(V2 16 7,V2 15 3),(V2 14 3,V2 15 3),(V2 20 1,V2 15 3)]
    it "should calculate radii" $ do
      toSensorRadius (parseInput sampleInput) `shouldBe`
          [(V2 2 18,V2 (-2) 15,7),(V2 9 16,V2 10 16,1),(V2 13 2,V2 15 3,3),(V2 12 14,V2 10 16,4),(V2 10 20,V2 10 16,4),(V2 14 17,V2 10 16,5),(V2 8 7,V2 2 10,9),(V2 2 0,V2 2 10,10),(V2 0 11,V2 2 10,3),(V2 20 14,V2 25 17,8),(V2 17 20,V2 21 22,6),(V2 16 7,V2 15 3,5),(V2 14 3,V2 15 3,1),(V2 20 1,V2 15 3,7)]
    it "should calculate dimensions" $ do
      findArea (toSensorRadius $ parseInput sampleInput) `shouldBe` (V2 (-8) (-10), V2 28 26)
    it "should calculate sample part 1" $ do
      solve 10 sampleInput `shouldBe` 26
    it "should calculate sample part 2" $ do
      [coord] <- solve2 20 sampleInput
      coord `shouldBe` V2 14 11
      toTuningFreq coord `shouldBe` 56000011

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d15.txt"
    it "---PART ONE---" $ do
      solve 2000000 input `shouldBe` 5688618
    fit "---PART TWO---" $ do
      [coord] <- solve2 4000000 input
      toTuningFreq coord `shouldBe` 12625383204261

type Coord = V2 Int

parseInput :: String -> [(Coord, Coord)]
parseInput = map (parseThrow inputP) . lines

toSensorRadius :: [(Coord, Coord)] -> [(Coord, Coord, Int)]
toSensorRadius = map $ \(sensor, beacon) -> (sensor, beacon, taxiDist sensor beacon)

toTuningFreq :: V2 Int -> Int
toTuningFreq = dot (V2 4000000 1)

taxiDist :: Coord -> Coord -> Int
taxiDist a b =
  let (V2 x y) = abs $ b - a
   in x + y

findArea :: [(Coord, Coord, Int)] -> (Coord, Coord)
findArea = foldl f (V2 0 0, V2 0 0)
  where
    f (V2 ltx lty, V2 rbx rby) (V2 sx sy, _, dist) =
      (V2 (min ltx (sx - dist)) (min lty (sy - dist)), V2 (max rbx (sx + dist)) (max rby (sy + dist)))

solve :: Int -> String -> Int
solve y input = do
  let sensorRadii = toSensorRadius $ parseInput input
      (V2 minx _, V2 maxx _) = findArea sensorRadii
      canHasBeacon = length . filter id . (`map` [minx..maxx]) $ \x ->
        foldl (\acc (s, b, r) -> (taxiDist (V2 x y) s <= r && V2 x y /= b) || acc) False sensorRadii
  canHasBeacon

solve2 :: Int -> String -> IO [Coord]
solve2 maxCoord input = do
  print sensorRadii
  let coordsList = map (, 1 :: Int) . join $ map (\(sensor, _, radius) -> findPerimeter sensor radius) sensorRadii
      -- build histogram
      coordsMap = M.fromListWith (+) coordsList
      -- select only potential coordinates which belong to more than 4 perimeters
      coords = map fst . filter (\(_, n) -> n >= 4) $ toList coordsMap
  nCpus <- coords `seq` getNumCapabilities
  result <- pooledForConcurrentlyN nCpus coords $ \coord -> do
    let isBeacon =
          foldl'
            (\acc (s, _, r) -> (taxiDist coord s > r) && acc)
            True sensorRadii
    if isBeacon
       then purer coord
       else pure Nothing
  pure $ catMaybes result
    where
      sensorRadii = toSensorRadius $ parseInput input
      isWithinBounds (V2 x y) = 0 <= x && x <= maxCoord && 0 <= y && y <= maxCoord
      findPerimeter sensor@(V2 sx sy) radius = snd $ execState findPerimeterS (startingPoint, [])
        where
          startingPoint = V2 sx (sy - radius - 1)
          nextPerimeterPoint point = do
           let (V2 dx dy) = point - sensor
           point +
             if | dx >= 0 && dy > 0 -> V2 1 (-1)
                | dx > 0 && dy <= 0 -> V2 (-1) (-1)
                | dx <= 0 && dy < 0 -> V2 (-1) 1
                | dx < 0 && dy >= 0 -> V2 1 1
                | otherwise         -> error $ "UNKNOWN CASE: " <> show (V2 dx dy)
          findPerimeterS = do
           (p, ps) <- get
           let p' = nextPerimeterPoint p
           put $ if isWithinBounds p
              then (p', p:ps)
              else (p', ps)
           when (p' /= startingPoint)
            findPerimeterS

inputP :: Parser (Coord, Coord)
inputP = do
  _ <- string "Sensor at x="
  sx <- signedInteger
  _ <- string ", y="
  sy <- signedInteger
  _ <- string ": closest beacon is at x="
  bx <- signedInteger
  _ <- string ", y="
  by <- signedInteger
  pure (V2 sx sy, V2 bx by)
    where
      lexeme'        = L.lexeme space
      integer       = lexeme' L.decimal
      signedInteger = L.signed space integer

sampleInput :: String
sampleInput = [qc|Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3|]
