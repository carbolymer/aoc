module Y2022.D06Spec where

import Common
import Test.Hspec
import Text.InterpolatedString.Perl6
import Data.Set qualified as S
import GHC.Exts
import Control.Arrow

spec :: Spec
spec = do
  describe "Validate sample input" $ do
    it "sample part 1" $ do
      map (fst . findMarker 4 . buildSubStrings 4) sampleInput `shouldBe` [7,5,6,10,11]
    it "sample part 2" $ do
      map (fst . findMarker 14 . buildSubStrings 14) sampleInput `shouldBe` [19,23,23,29,26]

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d06.txt"
    it "---PART ONE---" $ do
      (fst . findMarker 4 . buildSubStrings 4 ) input  `shouldBe` 1625
    it "---PART TWO---" $ do
      (fst . findMarker 14 . buildSubStrings 14) input  `shouldBe` 2250

findMarker :: HasCallStack => Int -> [String] -> (Int, String)
findMarker markerSize = first (+ markerSize) . head . filter ((== markerSize) . S.size . fromList . snd) . zip [0..]

buildSubStrings :: Int -> String -> [String]
buildSubStrings _ [] = []
buildSubStrings len input@(_:rest)
  | length input > len = take len input : buildSubStrings len rest
  | otherwise = []


sampleInput :: [String]
sampleInput =
  [ "mjqjpqmgbljsphdztnvjfqwrcgsmlb"
  , "bvwbjplbgvbhsrlpgdmjqwftvncz"
  , "nppdvjthqldpwncqszvftbrmjlhg"
  , "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
  , "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"]
