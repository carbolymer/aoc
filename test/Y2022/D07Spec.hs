module Y2022.D07Spec where

import Common
import Control.Applicative.Combinators
import Control.Arrow
import Control.Monad.State.Strict
import Data.Foldable                   (for_)
import Data.List                       (sort)
import Data.List.Extra                 (stripPrefix)
import Data.Map.Strict                 (Map)
import Data.Map.Strict                 qualified as M
import Data.Maybe                      (fromJust)
import Data.Set                        (Set)
import Data.Set                        qualified as S
import Debug.Trace
import GHC.Exts
import Test.Hspec
import Text.InterpolatedString.Perl6
import Text.Megaparsec                 (try)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer      qualified as L

spec :: Spec
spec = do
  describe "test inputParser" $ do
    it "try parse cd cmd" $ do
      parseThrow cdCmdP "$ cd /\n" `shouldBe` Cd (Dir "/")
    it "try parse ls cmd" $ do
      parseThrow lsCmdP "$ ls\ndir a\n123 b.txt\n" `shouldBe` Ls [DirI "a", FileI "b.txt" 123]
    it "try parse sample input" $ do
      parseThrow cliOutputP sampleInput `shouldBe`
        [Cd $ Dir "/"
        , Ls [ DirI "a"
             , FileI "b.txt" 14848514
             , FileI "c.dat" 8504156
             , DirI "d" ]
        , Cd $ Dir "a"
        , Ls [ DirI "e"
             , FileI "f" 29116
             , FileI "g" 2557
             , FileI "h.lst" 62596 ]
        , Cd $ Dir "e"
        , Ls [ FileI "i" 584 ]
        , Cd Parent
        , Cd Parent
        , Cd $ Dir "d"
        , Ls [ FileI "j" 4060174
             , FileI "d.log" 8033020
             , FileI "d.ext" 5626152
             , FileI "k" 7214296 ]]
  describe "Validate sample input" $ do
    it "build sample tree" $ do
      buildFileTree (parseThrow cliOutputP sampleInput)
      `shouldBe`
        DirNode [
          ("/", DirNode
            [ ("a", DirNode
              [ ("e", DirNode
                  [("i",FileNode 584)])
              , ("f", FileNode 29116)
              , ("g", FileNode 2557)
              , ("h.lst", FileNode 62596)])
            , ("b.txt", FileNode 14848514)
            , ("c.dat", FileNode 8504156)
            , ("d", DirNode
              [ ("j", FileNode 4060174)
              , ("d.ext", FileNode 5626152)
              , ("d.log", FileNode 8033020)
              , ("k", FileNode 7214296)])])
            ]
    it "should list dirs with their contents" $ do
      listDirs (buildFileTree (parseThrow cliOutputP sampleInput))
        `shouldBe` [("/",48381165),("/a",94853),("/a/e",584),("/d",24933642)]
    it "sample part 1" $ do
      (sum . filter (< 100000) $ map snd (listDirs (buildFileTree (parseThrow cliOutputP sampleInput))))
        `shouldBe` 95437

    it "sample part 2" $ do
      let dirsSizes = map snd . listDirs . buildFileTree $ parseThrow cliOutputP sampleInput
          totalUsedSpace = head dirsSizes
          dirsSizesSorted = sort dirsSizes
          minDelete = 30_000_000 - (70_000_000 - totalUsedSpace)
          toDelete = head $ filter (> minDelete) dirsSizesSorted
      toDelete `shouldBe` 24933642


  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d07.txt"
    it "---PART ONE---" $ do
      (sum . filter (< 100000) $ map snd (listDirs (buildFileTree (parseThrow cliOutputP input))))
        `shouldBe` 1792222
    it "---PART TWO---" $ do
      let dirs = listDirs . buildFileTree $ parseThrow cliOutputP input
          dirsSizes = map snd dirs
          totalUsedSpace = head dirsSizes
          dirsSizesSorted = sort dirsSizes
          minDelete = 30_000_000 - (70_000_000 - totalUsedSpace)
          toDelete = head $ filter (>= minDelete) dirsSizesSorted
      toDelete `shouldBe` 1112963

type Name = String
type Size = Int

data Dir = Dir Name
         | Parent
         deriving (Show, Ord, Eq)

data Cmd = Cd Dir
         | Ls [DirItem]
  deriving (Show, Ord, Eq)

data DirItem = FileI Name Size | DirI Name
  deriving (Show, Ord, Eq)

-- {{{ PARSER
cliOutputP :: Parser [Cmd]
cliOutputP = some (try cdCmdP <|> try lsCmdP)

cdCmdP :: Parser Cmd
cdCmdP = do
  targetDir <- char '$' >> hspace >> string "cd" >> hspace >> dirNameP <* eol
  pure $ Cd targetDir
    where
      dirNameP = (Dir <$> some (alphaNumChar <|> char '/')) <|> (Parent <$ string "..")


lsCmdP :: Parser Cmd
lsCmdP = do
  _ <- char '$' >> hspace >> "ls" >> eol
  Ls <$> dirItemP `endBy` eol

dirItemP :: Parser DirItem
dirItemP = fileP <|> dirP
  where
    fileNameP = some $ alphaNumChar <|> char '.'
    fileP = do
      size <- L.decimal <* hspace
      name <- fileNameP
      pure $ FileI name size
    dirP = DirI <$> (string "dir " >> fileNameP)
-- }}}

data FileTree
  = DirNode (Map Name FileTree)
  | FileNode Size
  deriving (Eq, Ord, Show)

buildFileTree :: [Cmd] -> FileTree
buildFileTree = snd . foldl readCmd ([], DirNode mempty)
  where
    readCmd (path, tree) = \case
      Cd Parent        -> (tail path, tree)
      Cd (Dir dirName) -> (dirName : path, tree)
      Ls dirItems      -> (path, treeInsertAt path dirItems tree)

treeInsertAt :: [Name] -> [DirItem] -> FileTree -> FileTree
treeInsertAt path items srcTree = flip execState srcTree $ do
  let path' = reverse path
  insertItemsAt path' items
    where
      insertItemsAt :: [Name] -> [DirItem] -> State FileTree ()
      insertItemsAt [] items' = do
        get >>= \case
          DirNode contents ->
            put . DirNode . flip execState contents $ do
              for_ items' $ \case
                FileI name size -> modify' $ M.insert name (FileNode size)
                DirI name       -> modify' $ M.insert name (DirNode mempty)
          n -> error $ "invalid insert state: " <> show n
      insertItemsAt (p:ps) items' = do
        get >>= \case
          DirNode contents -> do
            case M.lookup p contents of
              Just (FileNode _)  -> error $ "cannot cd into file: " <> show p
              Just d@(DirNode _) -> put d
              Nothing            -> put $ DirNode mempty
            insertItemsAt ps items'
            modify' $ \s' -> DirNode (M.insert p s' contents)
          n -> error $ "invalid insert state: " <> show n

listDirs :: FileTree -> [(Name, Size)]
listDirs (FileNode _) = []
listDirs (DirNode contents') = go "" contents'
  where
    go :: Name -> Map Name FileTree -> [(Name, Size)]
    go prefixPath contents = do
      let pp = case prefixPath of
                 ""   -> ""
                 "/"  -> ""
                 "//" -> ""
                 _    -> prefixPath
          pp' = if null pp then "/" else pp
          totalSize = sum [size | (_, FileNode size) <- M.assocs contents]
          dirs = join [go (pp <> "/" <> name) c' | (name , DirNode c') <- M.assocs contents]
          onlyDirectSubdirs = filter ((== 1) . countOccurrences '/' . fst) $ map (first (fromJust . stripPrefix pp)) dirs
          totalSizeWithSubDirs = totalSize + sum (map snd onlyDirectSubdirs)
      if null prefixPath
         then dirs
         else (pp', totalSizeWithSubDirs) : dirs

sampleInput :: String
sampleInput = [qc|$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
|]
