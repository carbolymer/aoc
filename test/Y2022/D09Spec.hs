{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ImpredicativeTypes #-}
module Y2022.D09Spec where

import Common
import Control.Lens
import Control.Lens.TH
import Control.Monad.State.Strict
import Data.List                     (nub)
import Data.Set                      qualified as S
import GHC.Exts
import Test.Hspec
import Text.InterpolatedString.Perl6
import Debug.Trace

sampleInput :: String
sampleInput = [qc|R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2|]

data Move = L Int | U Int | R Int | D Int
  deriving (Show, Eq, Ord, Read)

readInput :: String -> [Move]
readInput = map read . lines

data Position = Position
  { _x :: Int
  , _y :: Int
  } deriving (Eq, Ord)

instance Show Position where
  show (Position x y) = "(" <> show x <> "," <> show y <> ")"

data MoveState = MoveState
  { _rope      :: [Position] -- rope knots
  , _positions :: [Position]
  } deriving Show

$(makeLenses ''Position)
$(makeLenses ''MoveState)


newKnotState :: Int -> MoveState
newKnotState nKnots = MoveState (replicate nKnots $ Position 0 0) []

performRopeMoves :: Int -> [Move] -> MoveState
performRopeMoves nKnots moves = (`execState` newKnotState nKnots) $ mapM_ moveHead moves

moveHead :: Move -> State MoveState ()
moveHead (L 0) = pure ()
moveHead (U 0) = pure ()
moveHead (R 0) = pure ()
moveHead (D 0) = pure ()
moveHead move@(L dist) = do
  makeMove move subtract
  moveHead (L (dist - 1))

moveHead move@(U dist) = do
  makeMove move (+)
  moveHead (U (dist - 1))

moveHead move@(R dist) = do
  makeMove move (+)
  moveHead (R (dist - 1))

moveHead move@(D dist) = do
  makeMove move subtract
  moveHead (D (dist - 1))

directions :: Move -> Lens' Position Int
directions (U _) = y
directions (D _) = y
directions (R _) = x
directions (L _) = x

makeMove :: Move -> (Int -> Int -> Int) -> State MoveState ()
makeMove move g = do
  let headMove = f %~ g 1 $ Position 0 0
  doMoveRope 0 headMove
  registerPosition
    where
      f = directions move
      doMoveRope :: Int -> Position -> State MoveState ()
      doMoveRope n (Position x' y') = do
        -- (_, wasDiagonal) <- knotDistance n
        -- origHead <- (^?! element n) <$> use rope
        --
        -- -- move head actually
        -- when (n == 0) $
        --   (rope . element 0 . f) %= g 1
        --
        -- (tailDistance', _) <- knotDistance n
        --
        -- if wasDiagonal && tailDistance' > 1
        --    then rope . element (n+1) .= origHead
        --    else do
        --     when (tailDistance' > 1) $
        --       (rope . element (n+1) . f) %= g 1

        when (n == 0) $
          (rope . element 0 . f) %= g 1

        (tailDistance', Position x'' y'') <- {- traceShowId <$> -} knotDistance n
        when (tailDistance' > 1) $ do
          (rope . element (n+1) . x) %= (subtract x' . (+ x''))
          (rope . element (n+1) . y) %= (subtract y' . (+ y''))

        rope' <- use rope
        when ((n + 2) < length rope') $
          doMoveRope (n+1) (Position (x'' - x') (y'' - y'))

registerPosition :: State MoveState ()
registerPosition = do
  pos <- last <$> use rope
  positions %= (pos :)

knotDistance :: Int -> State MoveState (Int, Position)
knotDistance n = do
  Position headX headY <- (^?! element n) <$> use rope
  Position tailX tailY <- (^?! element (n + 1)) <$> use rope
  let (x', y') = (headX - tailX, headY - tailY)
  pure (max (abs x') (abs y'), Position x' y')

spec :: Spec
spec = do
  describe "validate sample input" $ do
    it "read input" $ do
      readInput sampleInput `shouldBe` [R 4,U 4,L 3,D 1,R 4,D 1,L 5,R 2]
    it "verify trail" $ do
      (_positions . performRopeMoves 2 $ readInput sampleInput) `shouldBe`
        map (uncurry Position) [(1,2),(1,2),(1,2),(2,2),(3,2),(4,3),(4,3),(4,3),(4,3),(3,3),(2,4),(2,4),(2,4),(2,4),(3,4),(4,3),(4,3),(4,2),(4,1),(3,0),(3,0),(2,0),(1,0),(0,0)]
    it "sample part 1" $ do
      (length . nub . _positions . performRopeMoves 2 $ readInput sampleInput) `shouldBe` 13
    it "trail part 2" $ do
      (_positions . performRopeMoves 10 $ readInput sampleInput) `shouldBe`
          map (uncurry Position) [(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0)]

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d09.txt"
    it "---PART ONE---" $ do
      (length . nub . _positions . performRopeMoves 2 $ readInput input) `shouldBe` 6332
    it "---PART TWO---" $ do
      (length . nub . _positions . performRopeMoves 10 $ readInput input) `shouldBe` 4880
