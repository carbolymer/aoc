module Y2022.D05Spec where

import Common                        hiding (count)
import Data.Bifunctor                (bimap)
import Data.List                     (intercalate)
import Data.List.Split               (splitWhen)
import Debug.Trace
import Test.Hspec
import Text.InterpolatedString.Perl6
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer    qualified as L

spec :: Spec
spec = do
  describe "Validate sample input" $ do
    describe "parses row" $ do
      it "all fields present" $ do
        parseThrow stackLineP "[Z] [M] [P]"
          `shouldBe` [Just 'Z',Just 'M', Just 'P']
      it "middle missing" $ do
        parseThrow stackLineP "[Z]     [P]"
          `shouldBe` [Just 'Z', Nothing, Just 'P']
      it "two last missing" $ do
        parseThrow stackLineP "[Z]        "
          `shouldBe` [Just 'Z', Nothing, Nothing]
      it "last missing" $ do
        parseThrow stackLineP "[Z] [M]    "
          `shouldBe` [Just 'Z',Just 'M', Nothing]
    it "parse single field - empty space" $ do
      parseThrow ((Nothing <$ count 3 (char ' ')) :: Parser (Maybe Bool)) "   "
        `shouldBe` Nothing
    describe "test sample" $ do
      it "validate parsing" $ do
        readInput sampleInput
          `shouldBe` (["NZ", "DCM", "P"],
              [ Move { from = 2, to = 1, amount = 1 }
              , Move { from = 1, to = 3, amount = 3 }
              , Move { from = 2, to = 1, amount = 2 }
              , Move { from = 1, to = 2, amount = 1 }])
      it "sample part 1" $ do
        let (stacks, actions) = readInput sampleInput
        getMessage (runActions actions stacks)
          `shouldBe` "CMZ"
      it "sample part 2" $ do
        let (stacks, actions) = readInput sampleInput
        getMessage (runActions2 actions stacks)
          `shouldBe` "MCD"
  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d05.txt"
    it "---PART ONE---" $ do
      let (stacks, actions) = readInput input
      getMessage (runActions actions stacks)
        `shouldBe` "RTGWZTHLD"
      True `shouldBe` True
    it "---PART TWO---" $ do
      let (stacks, actions) = readInput input
      getMessage (runActions2 actions stacks)
        `shouldBe` "STHGRZZFR"

type Stack = [Char]

data Action = Move { from :: Int, to :: Int, amount :: Int }
  deriving (Show, Eq)

runActions :: HasCallStack => [Action] -> [Stack] -> [Stack]
runActions [] stacks = stacks
runActions (Move{from, to, amount}:as) stacks = runActions as newStacks'
  where
    newStacks' = do
      let (_, source, _) = splitIntoThrow (from - 1) stacks
          (_, dest, _) = splitIntoThrow (to - 1) stacks
      updateAt (to - 1) (reverse (take amount source) <> dest) $
        updateAt (from - 1) (drop amount source) stacks

runActions2 :: HasCallStack => [Action] -> [Stack] -> [Stack]
runActions2 [] stacks = stacks
runActions2 (Move{from, to, amount}:as) stacks = runActions2 as newStacks'
  where
    newStacks' = do
      let (_, source, _) = splitIntoThrow (from - 1) stacks
          (_, dest, _) = splitIntoThrow (to - 1) stacks
      updateAt (to - 1) (take amount source <> dest) $
        updateAt (from - 1) (drop amount source) stacks

getMessage :: HasCallStack => [Stack] -> String
getMessage = map head

readInput :: String -> ([Stack], [Action])
readInput =
  bimap (parseThrow stacksP . intercalate "\n" . reverse) (parseThrow actionsP . intercalate "\n")
    . (\(a :: [[String]]) -> (head a, a !! 1))
    . splitWhen (== "")
    . lines

stacksP :: Parser [Stack]
stacksP = do
  _ <- some (hspace >> (L.decimal :: Parser Int) >> hspace) >> eol
  rows <- stackLineP `sepBy` eol
  pure $ foldl stackRows (replicate (length (head rows)) []) rows

stackLineP :: Parser [Maybe Char]
stackLineP = stackFieldP `sepBy` char ' '

stackFieldP :: Parser (Maybe Char)
stackFieldP = (Just <$> (char '[' *> asciiChar <* char ']')) <|> (Nothing <$ count 3 (char ' '))

stackRows :: [Stack] -> [Maybe Char] -> [Stack]
stackRows acc row = flip map (zip acc row) $ \(stack, char'm) -> case char'm of
                                                                   Just c  -> c : stack
                                                                   Nothing -> stack

actionsP :: Parser [Action]
actionsP = flip sepBy eol $ do
  amount <- symbol "move" *> L.decimal <* space
  from <- symbol "from" *> L.decimal <* space
  to <- symbol "to" *> L.decimal
  pure $ Move{..}
    where symbol = L.symbol space

sampleInput :: String
sampleInput = unlines
  [ "    [D]    "
  , "[N] [C]    "
  , "[Z] [M] [P]"
  , " 1   2   3"
  , ""
  , "move 1 from 2 to 1"
  , "move 3 from 1 to 3"
  , "move 2 from 2 to 1"
  , "move 1 from 1 to 2"]
