module Y2022.D08Spec where

import Common
import Data.List.Extra               (enumerate)
import Data.Word
import Prelude                       hiding (Left, Right)
import Test.Hspec
import Text.InterpolatedString.Perl6
import Debug.Trace

spec :: Spec
spec = do
  describe "test view checking" $ do
    it "visibility" $ do
      isVisibleFromEdge Top (0,0) (readArray sampleInput) `shouldBe` True
      isVisibleFromEdge Top (1,2) (readArray sampleInput) `shouldBe` False
    describe "view" $ do
      it "top" $
        maxViewToEdge Top (2, 3) (readArray sampleInput) `shouldBe` 2
      it "left" $
        maxViewToEdge Left (2, 3) (readArray sampleInput) `shouldBe` 2
      it "right" $
        maxViewToEdge Right (2, 3) (readArray sampleInput) `shouldBe` 2
  describe "validate sample input" $ do
    it "sample part 1" $ do
      countVisibleTrees (readArray sampleInput) `shouldBe` 21
    it "sample part 1" $ do
      scenicScore (2,3) (readArray sampleInput) `shouldBe` 8

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d08.txt"
    let treeMap = readArray input
    it "---PART ONE---" $ do
      countVisibleTrees treeMap `shouldBe` 1794
    it "---PART TWO---" $ do
      let idxs = treeMapAllIndices treeMap
      maximum (map (`scenicScore` treeMap) idxs) `shouldBe` 199272

readArray :: String -> [[Word8]]
readArray = map (map (read . pure)) . lines

countVisibleTrees :: [[Word8]] -> Int
countVisibleTrees treeMap = length . filter id . flip map (treeMapAllIndices treeMap) $ \coord -> isVisible coord treeMap

treeMapAllIndices :: Foldable t => [t a] -> [(Int, Int)]
treeMapAllIndices treeMap = [(x, y) | y <- [0 .. (length treeMap - 1)], x <- [0 .. (length (head treeMap) - 1)]]

isVisible :: (Int, Int) -> [[Word8]] -> Bool
isVisible coord treeMap = any (\edge -> isVisibleFromEdge edge coord treeMap) enumerate

scenicScore :: (Int, Int) -> [[Word8]] -> Int
scenicScore coord treeMap = product $ map (\edge -> maxViewToEdge edge coord treeMap) enumerate

data Edge = Left | Top | Right | Bottom
  deriving (Eq, Show, Ord, Enum, Bounded)

isVisibleFromEdge :: Edge -> (Int, Int) -> [[Word8]] -> Bool
isVisibleFromEdge edge (x, y) treeMap
  | x == 0 && edge == Left = True
  | y == 0 && edge == Top = True
  | x == maxX && edge == Right = True
  | y == maxY && edge == Bottom = True
  | otherwise = do
    let (startX, endX, startY, endY) =
          case edge of
            Left   -> (0, x-1, y, y)
            Top    -> (x, x, 0, y-1)
            Right  -> (x + 1, maxX, y, y)
            Bottom -> (x, x, y+1, maxY)
        treeHeight = treeMap !! y !! x
        treesToTheEdge = [treeMap !! yi !! xi | xi <- [startX..endX], yi <- [startY..endY], xi >= 0 && yi >= 0 && xi <= maxX && yi <= maxY]
    all (< treeHeight) treesToTheEdge
      where
        maxX = length (head treeMap) - 1
        maxY = length treeMap - 1

maxViewToEdge :: Edge -> (Int, Int) -> [[Word8]] -> Int
maxViewToEdge edge (x, y) treeMap
  | x == 0 && edge == Left = 0
  | y == 0 && edge == Top = 0
  | x == maxX && edge == Right = 0
  | y == maxY && edge == Bottom = 0
  | otherwise = do
    let (startX, endX, startY, endY) =
          case edge of
            Left   -> (x-1, 0, y, y)
            Top    -> (x, x, y-1, 0)
            Right  -> (x + 1, maxX, y, y)
            Bottom -> (x, x, y+1, maxY)
        treeHeight = treeMap !! y !! x
        treesToTheEdge = [treeMap !! yi !! xi | xi <- range startX endX, yi <- range startY endY, xi >= 0 && yi >= 0 && xi <= maxX && yi <= maxY]
    length $ visibleTrees treeHeight treesToTheEdge
      where
        maxX = length (head treeMap) - 1
        maxY = length treeMap - 1
        range a b
          | a <= b = [a..b]
          | otherwise = reverse [b..a]
        visibleTrees _ [] = []
        visibleTrees h (t:ts)
          | h <= t = [t]
          | otherwise = t:visibleTrees h ts


sampleInput :: String
sampleInput = [qc|30373
25512
65332
33549
35390|]
