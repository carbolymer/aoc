{-# LANGUAGE TemplateHaskell #-}
module Y2022.D12Spec where

import Common
import Control.Arrow
import Control.Lens
import Control.Monad.State.Strict
import Data.Char                     (ord)
import Data.Foldable
import Data.Hashable                 (Hashable)
import Data.HashPSQ                  (HashPSQ)
import Data.HashPSQ                  qualified as Q
import Data.Map.Strict               (Map)
import Data.Map.Strict               qualified as M
import Data.Maybe                    (catMaybes, fromJust, fromMaybe, mapMaybe)
import Data.Traversable              (for)
import Data.Word                     (Word8)
import Debug.Trace                   (traceShow, traceShowId)
import Linear                        (V2 (..))
import Linear.Metric
import Test.Hspec
import Text.InterpolatedString.Perl6
import UnliftIO                      (pooledForConcurrentlyN)

type Coord = V2 Int

data Heightmap = Heightmap
  { _currentPos :: Coord
  , _targetPos  :: Coord
  , _heightmap  :: [[Int]]
  } deriving (Show, Eq, Ord)
makeLenses ''Heightmap

currentHeight :: State Heightmap Int
currentHeight = do
  x <- use (currentPos . _1)
  y <- use (currentPos . _2)
  (!! x) . (!! y) <$> use heightmap

newMap :: Int -> Int -> Heightmap
newMap rowc colc = Heightmap (pure 0) (pure 0) $ replicate rowc (replicate colc 0)

readMap :: String -> Heightmap
readMap input = foldl' readRow (newMap rowCount colCount) . zip [0..] . map (zip [0..]) $ input'
  where
    input' = lines input
    V2 colCount rowCount = dimensions input'

    readRow :: Heightmap -> (Int, [(Int, Char)]) -> Heightmap
    readRow map' (y, row) = foldl' (readField y) map' row

    readField :: Int -> Heightmap -> (Int, Char) -> Heightmap
    readField y map' (x, height') = (`execState` map') $ do
      height <-
        case height' of
         'S' -> do
           currentPos .= V2 x y
           pure 'a'
         'E' -> do
           targetPos .= V2 x y
           pure 'z'
         h -> pure h
      heightmap . eAt (V2 x y) .= ord height

getHeight :: Coord -> State Heightmap Int
getHeight coord = fmap fromJust . preuse $ heightmap . eAt coord

dimensions :: Foldable t => [t a] -> Coord
dimensions input = do
  let rowCount = length input
      colCount = length (head input)
  V2 colCount rowCount

neighbours :: Coord -> State Heightmap [Coord]
neighbours pos@(V2 x y) = do
  V2 colc rowc <- dimensions <$> use heightmap
  h <- getHeight pos
  filterM
    (checkHeight h)
    [V2 x' y' | (x', y') <- [(x-1, y), (x+1, y), (x, y-1), (x, y+1) ]
              , x' >= 0 && y' >= 0 && x' < colc && y' < rowc ]
    where
      checkHeight h p = do
        h' <- getHeight p
        pure $ h + 1 >= h'

findAllAs :: Heightmap -> [V2 Int]
findAllAs Heightmap{_heightmap} = join . map reshape . map (second $ filter ((== 97) . snd)) . zip [0..] $ map (zip [0..]) _heightmap
  where
    reshape (y', ys) = (`map` ys) $ \(x', _) -> V2 x' y'

sampleInput :: String
sampleInput = [qc|Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi|]

spec :: Spec
spec = do
  describe "validate sample input" $ do
    it "read sample map" $ do
      readMap sampleInput `shouldBe`
         Heightmap {
           _currentPos = V2 0 0,
           _targetPos = V2 5 2,
           _heightmap = [
              [97, 97, 98, 113, 112, 111, 110, 109],
              [97, 98, 99, 114, 121, 120, 120, 108],
              [97, 99, 99, 115, 122, 122, 120, 107],
              [97, 99, 99, 116, 117, 118, 119, 106],
              [97, 98, 100, 101, 102, 103, 104, 105]]
         }
    it "find sample neighbours" $ do
      evalState (neighbours $ V2 2 4) (readMap sampleInput) `shouldBe`
        [V2 1 4, V2 3 4, V2 2 3]
    it "find sample neighbours 2" $ do
      evalState (neighbours $ V2 6 2) (readMap sampleInput) `shouldBe`
        [V2 7 2, V2 6 1, V2 6 3]
    it "find a's" $ do
      let hm = readMap sampleInput
      findAllAs hm `shouldBe` [V2 0 0,V2 1 0,V2 0 1,V2 0 2,V2 0 3,V2 0 4]

    it "find shortest path" $ do
      let hm@Heightmap{_currentPos, _targetPos} = readMap sampleInput
          getNeighbours coord = evalState (neighbours coord) hm `zip` repeat (1 :: Double)
          path = aStarSearch _currentPos _targetPos getNeighbours (distance (fromIntegral <$> _targetPos) . fmap fromIntegral)
      length <$> path `shouldBe` Just 31
      path `shouldBe`
        Just [V2 4 2,V2 4 1,V2 5 1,V2 6 1,V2 6 2,V2 6 3,V2 5 3,V2 4 3,V2 3 3,V2 3 2,V2 3 1,V2 3 0,V2 4 0,V2 5 0,V2 6 0,V2 7 0,V2 7 1,V2 7 2,V2 7 3,V2 7 4,V2 6 4,V2 5 4,V2 4 4,V2 3 4,V2 2 4,V2 2 3,V2 2 2,V2 2 1,V2 2 0,V2 1 0,V2 0 0]

    it "find best trail" $ do
      let hm@Heightmap{_currentPos, _targetPos} = readMap sampleInput
          as = findAllAs hm
      results <- fmap catMaybes . pooledForConcurrentlyN 20 as $ \startPos -> do
        let getNeighbours coord = evalState (neighbours coord) hm `zip` repeat (1 :: Double)
        pure $ length <$> aStarSearch startPos _targetPos getNeighbours (distance (fromIntegral <$> _targetPos) . fmap fromIntegral)
      minimum results `shouldBe` 29

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d12.txt"
    it "---PART ONE---" $ do
      let hm@Heightmap{_currentPos, _targetPos} = readMap input
          getNeighbours coord = evalState (neighbours coord) hm `zip` repeat (1 :: Double)
          path = aStarSearch _currentPos _targetPos getNeighbours (distance (fromIntegral <$> _targetPos) . fmap fromIntegral)
      length <$> path `shouldBe` Just 456
    it "---PART TWO---" $ do
      let hm@Heightmap{_currentPos, _targetPos} = readMap input
          as = findAllAs hm
      results <- fmap catMaybes . pooledForConcurrentlyN 20 as $ \startPos -> do
        let getNeighbours coord = evalState (neighbours coord) hm `zip` repeat (1 :: Double)
        pure $ length <$> aStarSearch startPos _targetPos getNeighbours (distance (fromIntegral <$> _targetPos) . fmap fromIntegral)
      minimum results `shouldBe` 454
