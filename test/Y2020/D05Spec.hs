module Y2020.D05Spec where

import           Common     hiding (count)
import           Data.List
import           Test.Hspec

data BoardingPass = BoardingPass [RowCode] [ColumnCode] deriving (Show)
data Seat = Seat { row :: Int, column :: Int, sid :: Int } deriving (Show, Eq)

data RowCode = F | B deriving (Show, Enum, Read, Eq)
data ColumnCode = L | R deriving (Show, Enum, Read, Eq)


decodeBinary :: Enum a => [a] -> Int
decodeBinary = foldl (\a v -> fromEnum v + 2*a) 0

readBoardingPass :: String -> BoardingPass
readBoardingPass boardingPassS = BoardingPass (map (read . (:[])) rowCodeS) (map (read . (:[])) columnCodeS)
  where
    (rowCodeS, columnCodeS) = if length boardingPassS /= 10
                                 then error $ "invalid boarding pass: " <> boardingPassS
                                 else splitAt 7 boardingPassS

calculateSeat :: BoardingPass -> Seat
calculateSeat (BoardingPass rowCode columnCode)= Seat
  { row = decodeBinary rowCode
  , column = decodeBinary columnCode
  , sid = (decodeBinary rowCode) * 8 + (decodeBinary columnCode)
  }

spec :: Spec
spec = do
  describe "part one boarding pass - decode seat" $ do
    it "tests reading rowcode" $ do
      read "F" `shouldBe` F
      read "B" `shouldBe` B
      (map (read . (:[])) "FBFBFB") `shouldBe` [F, B, F, B, F, B]
    it "tests binary decoding" $ do
      decodeBinary [F, F, F, F, F, F, F] `shouldBe` 0
      decodeBinary [F, F, F, F, F, F, B] `shouldBe` 1
      decodeBinary [F, B, F, B, B, F, F] `shouldBe` 44
    it "calculate sample seats" $ do
      (calculateSeat $ readBoardingPass "FBFBBFFRLR")
        `shouldBe` (Seat 44 5 357)
      (calculateSeat $ readBoardingPass "BFFFBBFRRR")
        `shouldBe` (Seat 70 7 567)
      (calculateSeat $ readBoardingPass "FFFBBBFRRR")
        `shouldBe` (Seat 14 7 119)
      (calculateSeat $ readBoardingPass "BBFFBBFRLL")
        `shouldBe` (Seat 102 4 820)
  describe "run puzzle input" $ do
    seats <- runIO $ (map (calculateSeat . readBoardingPass) . lines) <$> readFile "resources/2020/d05.txt"
    let sids = sort $ map (fromIntegral . sid) seats
    let maxSeatId = foldl max 0 sids :: Double
    let minSeatId = foldl min maxSeatId sids :: Double
    it "---PART ONE---" $ do
      putStrLn $ "highest seat id: " <> showI maxSeatId
      putStrLn $ "lowest seat id: " <> showI minSeatId
    it "---PART TWO---" $ do
      let nSeats = fromIntegral $ length seats
      let sumSeatsId = foldl (+) 0 sids
      let mySeat = ((maxSeatId + minSeatId)/2 * (nSeats + 1) - sumSeatsId)
      putStrLn $ "number of seats: " <> showI nSeats
      putStrLn $ "my missing seat: " <> showI mySeat
      let closeSeats = filter (\s -> s == mySeat || (s-1) == mySeat || (s+1) == mySeat) sids
      putStrLn $ "double check - try to find +1, -1 and my seat: " <> showI closeSeats
      -- putStrLn . unlines . map showI . sort $ sids

