module Y2020.D10Spec where
import           Common
import           Control.Monad
import           Control.Monad.Trans.Writer.Strict
import           Debug.Trace
import           Test.Hspec
import           Text.InterpolatedString.Perl6


findChain :: [Int] -> (Int, Int)
findChain inputChain = countDiffs . (\xs -> 0:xs) . head . filter hasAllAdapters $ (adapterChains 0 [] inputChain)
  where
    hasAllAdapters chain = length chain == length inputChain

    adapterChains _ currentChain [] = [currentChain]
    adapterChains joltage currentChain remainingChain = do
      (x, remainder) <- sampleSlices remainingChain
      when ((abs $ x - joltage) > 3) $ fail "too much joltage difference"
      adapterChains x (currentChain ++ [x]) remainder

    countDiffs :: [Int] -> (Int,Int)
    countDiffs []         = (0,1)
    countDiffs (x:[])     = (0,1)
    countDiffs (x1:x2:xs) = whichDiff x1 x2 `add` countDiffs (x2:xs)
    whichDiff x1 x2 = case (abs $ x1 - x2) of
                            1 -> (1,0)
                            3 -> (0,1)
                            _ -> (0,0)
    add (x1,x2) (y1,y2) = (x1+y1, x2+y2)

sampleSlices :: [a] -> [(a, [a])]
sampleSlices chain = execWriter (doSlice [] chain)
  where
    doSlice :: [a] -> [a] -> Writer [(a, [a])] ()
    doSlice _ [] = pure ()
    doSlice prepend (x:xs) = do
      tell [(x, prepend ++ xs)]
      doSlice (prepend ++ [x]) xs

testInput1 :: [Int]
testInput1 = map read . lines . trim $ [qc|16
10
15
5
1
11
7
19
6
12
4|]

testInput2 :: [Int]
testInput2 = map read . lines . trim $ [qc|28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3|]


spec :: Spec
spec = do
  describe "run test chain" $ do
    it "check sampleSlices" $ do
      sampleSlices [1,2,3,4] `shouldBe`
         [ (1,[2,3,4])
         , (2,[1,3,4])
         , (3,[1,2,4])
         , (4,[1,2,3])
         ]
      sampleSlices [1] `shouldBe` [ (1,[]) ]
    it "verify differences 1" $ do
      findChain testInput1 `shouldBe` (7, 5)
    it "verify differences 2" $ do
      findChain testInput2 `shouldBe` (22, 10)

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2020/d10.txt"
    it "---PART ONE---" $ do
      1 `shouldBe` 1
    it "---PART TWO---" $ do
      1 `shouldBe` 1

