module Y2020.D02Spec where
import           Common
import           Control.Monad
import           Data.Either
import           Test.Hspec
import           Text.Parsec   hiding (count)

data Password = Password Int Int Char String
  deriving (Show)

isValid :: Password -> Bool
isValid (Password minLen maxLen reqChar password) = reqCharCount >= minLen && reqCharCount <= maxLen
  where
    reqCharCount = countOccurrences reqChar password

isValid2 :: Password -> Bool
isValid2 (Password pos1 pos2 reqChar password) = condition1 `xor` condition2
  where
    condition1 = password !! (pos1-1) == reqChar
    condition2 = password !! (pos2-1) == reqChar

passwordParser :: Parsec String st Password
passwordParser = do
  minLen <- readInt <$> many1 digit
  _ <- char '-'
  maxLen <- readInt <$> many1 digit
  _ <- char ' '
  reqChar <- letter
  _ <- string ": "
  password <- many1 letter
  pure $ Password minLen maxLen reqChar password

spec :: Spec
spec = describe "runs second day" $ do
  it "" $ do
    input <- lines <$> readFile "resources/2020/d02.txt"
    let (failures, passwords) = partitionEithers . map (parse passwordParser "") $ input
    when (length failures > 0) . putStrLn $ "parsing failed: " <> show failures
    putStrLn $ "valid passwords - part one: " <> show (length $ filter isValid passwords)
    putStrLn $ "valid passwords - part two: " <> show (length $ filter isValid2 passwords)



