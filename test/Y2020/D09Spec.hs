module Y2020.D09Spec where
import           Control.Monad
import           Data.Maybe
import           Test.Hspec
import           Text.InterpolatedString.Perl6

readInput :: String -> [Integer]
readInput = map read . lines

testInput :: String
testInput = [qc|35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576|]

validateXmas :: Int -> [Integer] -> Maybe Integer
validateXmas preambleSize input = toResult $ foldM checkNumber (take preambleSize input) (drop preambleSize input)
  where
    checkNumber :: [Integer] -> Integer -> Either Integer [Integer]
    checkNumber preamble number = if number `elem` [ x + y | x <- preamble, y <- preamble, x /= y]
                                     then Right $ (tail preamble ++ [number])
                                     else Left number
    toResult (Right _)    = Nothing
    toResult (Left value) = Just value

findSet :: Integer -> [Integer] -> Maybe [Integer]
findSet theNumber input = toResult $ foldM checkNumber (take 2 input) (drop 2 input)
  where
    checkNumber :: [Integer] -> Integer -> Either [Integer] [Integer]
    checkNumber testSet number
      | length testSet < 2 = Right $ testSet ++ [number]
      | sum testSet < theNumber = Right $ testSet ++ [number]
      | sum testSet == theNumber = Left $ testSet
      | sum testSet > theNumber = checkNumber (tail testSet) number
    toResult (Right _)    = Nothing
    toResult (Left value) = Just value

spec :: Spec
spec = do
  describe "runs test data" $ do
    let testData  = readInput testInput
    let result = 127
    it "verifies test data" $ do
      validateXmas 5 testData `shouldBe` Just result
    it "verifies test data part two" $ do
      let matchingSet = fromJust $ findSet result testData
      (minimum matchingSet + maximum matchingSet) `shouldBe` 62


  describe "run puzzle input" $ do
    input <- runIO . fmap readInput $ readFile "resources/2020/d09.txt"
    let result = 70639851
    it "---PART ONE---" $ do
      validateXmas 25 input `shouldBe` Just result
    it "---PART TWO---" $ do
      let matchingSet = fromJust $ findSet result input
      (minimum matchingSet + maximum matchingSet) `shouldBe` 8249240
