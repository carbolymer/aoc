{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE TemplateHaskell      #-}
{-# LANGUAGE UndecidableInstances #-}
module Common where

import Control.Arrow
import Control.Lens
import Control.Monad.State.Strict
import Data.Aeson                 qualified as A
import Data.ByteString.Lazy       (ByteString)
import Data.Char                  (isAsciiLower, isSpace)
import Data.Hashable              (Hashable)
import Data.HashPSQ               (HashPSQ)
import Data.HashPSQ               qualified as Q
import Data.Map.Strict            (Map)
import Data.Map.Strict            qualified as M
import Data.Maybe                 (catMaybes, fromJust, fromMaybe)
import Data.Traversable           (for)
import Data.Void
import Data.Word                  (Word8)
import Debug.Trace                (traceShow, traceShowId)
import GHC.Stack                  (HasCallStack, withFrozenCallStack)
import Linear
import Linear.Metric
import Text.Megaparsec
import UnliftIO                   (pooledForConcurrentlyN)

uncurry3 :: (a -> b -> c -> d) -> (a,b,c) -> d
uncurry3 f (a,b,c) = f a b c

mul3 :: Num a => a -> a -> a -> a
mul3 a b c = a * b * c

readInt :: String -> Int
readInt = read

class ShowI a where
  showI :: a -> String

instance {-# OVERLAPPABLE #-} RealFrac a => ShowI a where
  showI = (show :: Integer -> String) . truncate

instance ShowI a => ShowI [a] where
  showI = show . map showI

countOccurrences :: Eq a => a -> [a] -> Int
countOccurrences x = length . filter (==x)

xor :: Bool -> Bool -> Bool
xor True True   = False
xor False True  = True
xor True False  = True
xor False False = False

letters :: String -> String
letters = filter isAsciiLower

update :: [a] -> Int -> a -> [a]
update initList idx element = updateWith initList idx (const element)

updateWith :: [a] -> Int -> (a -> a) -> [a]
updateWith initList idx f  = x' ++ f x : xs
  where (x',x:xs) = splitAt idx initList

trim :: String -> String
trim = f . f
  where f = reverse . dropWhile isSpace

{-# NOINLINE [1] applyN'#-}
applyN' :: Int -> (a -> a) -> a -> a
applyN' 0 _ x = x
applyN' n f x =
    let x' = f x
    in x' `seq` applyN' (n - 1) f x'

histogram :: (Ord a, Eq a) => [a] -> M.Map a Int
histogram [element]      = M.singleton element 1
histogram (element:elts) = M.insertWith (+) element 1 $ histogram elts

splitIntoThrow :: HasCallStack => Int -> [a] -> ([a], a, [a])
splitIntoThrow n arr = (take n arr, arr !! n, reverse . take (length arr - n - 1) $ reverse arr)

updateAt :: Int -> a -> [a] -> [a]
updateAt i a ls
  | i < 0 = ls
  | otherwise = go i ls
  where
    go 0 (_:xs) = a : xs
    go n (x:xs) = x : go (n-1) xs
    go _ []     = []
{-# INLINE updateAt #-}

type Parser = Parsec Void String

parseThrow :: (HasCallStack, ShowErrorComponent e) => Parsec e String a -> String -> a
parseThrow parser = _fromRight . parse parser ""
  where
    _fromRight (Right a) = a
    _fromRight (Left e)  = withFrozenCallStack $ error $ "parser error:\n" <> errorBundlePretty e


data PathSearch e d = PathSearch
  { -- | Set of discovered nodes, sorted by priority 'p'
    _openSet  :: HashPSQ e d ()
    -- | Map of cheapest paths, value is the node preceding
  , _cameFrom :: Map e e
    -- | Cheapest scores
  , _gScore   :: Map e d
  }
makeLenses ''PathSearch

aStarSearch :: (Foldable t, Eq e, Ord e, Hashable e, Num d, Ord d)
            => e -- ^ Starting element
            -> e -- ^ Search target
            -> (e -> t (e, d)) -- ^ return neighbours - 'd' is a distance
            -> (e -> d) -- ^ target distance heuristic
            -> Maybe [e] -- ^ path to target if successfull
aStarSearch start goal getNeighbours heuristic = evalState go PathSearch
  { _openSet = Q.singleton start (heuristic start) ()
  , _cameFrom = mempty
  , _gScore = M.singleton start 0
  }
    where
      go = (Q.findMin <$> use openSet) >>= \case
        Nothing -> pure Nothing
        Just (current, currentScore, _)
          | current == goal -> Just <$> buildPath current
          | otherwise -> do
              openSet %= Q.delete current
              forM_ (getNeighbours current) $ \(neighbour, distance') -> do
                let tentativeScore = currentScore + distance'
                neighbourScore <- gScoreS neighbour
                when (tentativeScore < neighbourScore) $ do
                  cameFrom %= M.insert neighbour current
                  gScore %= M.insert neighbour tentativeScore
                  openSet %= Q.insert neighbour (tentativeScore + heuristic neighbour) ()
              go
      gScoreS node = fromMaybe 100_000_000_000 <$> use (gScore . at node)
      buildPath node = use (cameFrom . at node) >>= \case
        Nothing       -> pure []
        Just fromNode -> (fromNode:) <$> buildPath fromNode


decodeFail ::  A.FromJSON a => ByteString -> a
decodeFail input = case A.eitherDecode input of
                     Right r -> r
                     Left e  -> error e


eAt :: (Index m ~ Index (IxValue m), Applicative f, Ixed m, Ixed (IxValue m)) => V2 (Index m) -> (IxValue (IxValue m) -> f (IxValue (IxValue m))) -> m -> f m
eAt (V2 x y) = ix y . ix x
